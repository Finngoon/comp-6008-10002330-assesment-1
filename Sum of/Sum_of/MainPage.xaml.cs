﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sum_of
{
    public partial class MainPage : ContentPage
    {
        static string q1 = "Your Answer is:" ;
        public MainPage()
        {
            InitializeComponent();
        }
        void Button(object sender, EventArgs e)
        {
            var a = new CalculateTask1();
            var b = a.clicked1(entry.Text);

            answer.Text = $"{q1} {b}";
        }

    }
}
